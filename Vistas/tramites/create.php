<?php include('../template/app.php')?>
<div class="container">

    
    
   <div class="row">
        <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <h1>Nuevo Tramite</h1>
                <hr>
                <form id="frmDatos" role="form" action="store.php" method="POST">
            
                        <div class="form-group">
                            <label for="numero">Numero:</label>
                            <input type="number" class="form-control" id="numero" name="numero" placeholder="" required>
                        </div>

                        <div class="form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" required>
                        </div>
                        <button id="btnGuardar" class="btn btn-default"> Guardar</button>
                </form>
                <div id="result">   </div>
                <div id="grilla"></div>

            </div>
        <div class="col-sm-2"></div>        
    </div>
</div>


<?php include('../template/footer.php')?>
<script type ="text/javascript">

$(document).ready(function()
{
    $("#grilla").load("table.php"); // ME MUESTRA ANTES QUE GUARDE LA GRILLA CON LOS DATOS
    
    $("#frmDatos").submit( function(event)
    {
        event.preventDefault();
        $.ajax(
        {
            type: "POST",
            url: "store.php",
            data: $("#frmDatos").serialize(),
            success: function(res)
            {
                if($.trim(res)==="ok") // trim para eliminar espacios de res
                {
                    $("#result").html("Se Inserto correctamente");
                        //cargando en la grilla en el div grilla
                        $("#grilla").load("table.php");
                }
                else
                    $("#result").html(res);
            }
       });
    });
});

</script>
