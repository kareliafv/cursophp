<?php

include('../../rutas/route.php');

    $obj = new TramiteController();
    $tramites = $obj->listar();
?>
<p>

    <button class="btn btn-info" onclick="window.print()">Imprimir</button>
</p>
<!-- Table -->
<table class="table">
    <tr>
        <th>ID</th>
        <TH>NUMERO</TH>
        <TH>NOMBRE</TH>
        <th>Opciones</th>
    </tr>


    <?php while($row=$tramites->fetch_object()):?>

        <tr>
            <td><?php echo $row->id ?></td>
            <td><?php echo $row->numero ?></td>
            <td><?php echo $row->descripcion ?></td>
            <td>
            <td>
                <a href="edit.php?di=<?php echo $row->id?>"class="btn btn-sm btn-info"> <span class = "glyphicon glyphicon-edit"></span> Modificar </a>
                <a href="eliminar.php?di=<?php echo $row->id?>"class="btn btn-sm btn-danger" onclick="return confirm ('esta seguro')"> <span class = "glyphicon glyphicon-remove"></span>eliminar </a>
                <a href="show.php?di=<?php echo $row->id?>"class="btn btn-sm btn-default"> <span class = "glyphicon glyphicon-eye-open"></span> Ver </a>
            </td>
        </tr>

    <?php endwhile; ?>
</table>