<?php
    include('../../rutas/route.php');
    //recogiendo variable de la url con GET
    $id=$_GET['di'];

    $obj = new TramiteController();
   if($obj->eliminar($id)):
?>
<div class="container">
    <?php include('../template/app.php')?>
        <div class="alter alter-success">Registro eliminado Correctamente !!!</div>
    <?php else: ?>
        <div class="alter alter-danger">Ha ocurrido un error, vuelva a intentarlo</div>
    <?php endif; ?>
    <button class="btn btn-lg btn-primary" onclick="history.back(-1)">Volver...</button>
</div>

<?php include('../template/footer.php')?>