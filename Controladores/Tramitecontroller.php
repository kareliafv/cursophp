<?php
class TramiteController
{
    private function ejecutar_consulta($query)
    {
        $cadcon = Conexion :: conectar();
        $res = $cadcon->query($query);
        conexion::desconectar($cadcon);
        return $res;
    }
    public function  listar()
    {
        $query = "SELECT * FROM tramite";
        return $this->ejecutar_consulta($query);
    }

    public function mostrar($id)
    {
        $query = "SELECT * FROM tramite WHERE id = '$id'";
        return $this->ejecutar_consulta($query);
    }
    public function eliminar($id)
    {
        $query = "DELETE FROM tramite WHERE id = '$id' ";
        return $this->ejecutar_consulta($query);
                          
    }
    public function modificar($reg,$id)
    {
        $query = "UPDATE tramite SET numero = '$reg->numero', descripcion = '$reg->nombre' WHERE id = '$id'";
        return $this->ejecutar_consulta($query);
    }

    public function insertar($reg)
    {
        $query = "INSERT INTO tramite VALUES(null,'$reg->numero','$reg->nombre')";
        return $this->ejecutar_consulta($query);
    }
}    

   /* $obj = new TramiteController();
    $tramites = $obj->listar();
    print_r($tramites);*/

