<?php

error_reporting(E_ALL);

CLASS Conexion
{
    public static function conectar()
    {
        $server="localhost";
        $database="curso";
        $user="root";
        $pasword="";
        $cadcon=new mysqli($server, $user, $pasword);
        $cadcon->select_db($database);
        if($cadcon)
        return $cadcon;
        else
        return
        null;
    }

    public static function desconectar($cadcon)
    {
        mysqli_close($cadcon);
    }
}